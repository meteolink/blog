import os
import shutil
import sys
from socketserver import TCPServer

from fabric.api import *

from pelican.server import ComplexHTTPRequestHandler

env.deploy_path = 'output'
DEPLOY_PATH = env.deploy_path

# Port for `serve`
PORT = 8000

@task()
def clean():
    """Remove generated files"""
    if os.path.isdir(DEPLOY_PATH):
        shutil.rmtree(DEPLOY_PATH)
        os.makedirs(DEPLOY_PATH)

@task()
def build():
    """Build local version of site"""
    local('pelican -s pelicanconf.py')

@task()
def rebuild():
    """`build` with the delete switch"""
    local('pelican -d -s pelicanconf.py')

@task()
def regenerate():
    """Automatically regenerate site upon file modification"""
    local('pelican -r -s pelicanconf.py')

@task()
def serve():
    """Serve site at http://localhost:8000/"""
    os.chdir(env.deploy_path)

    class AddressReuseTCPServer(TCPServer):
        allow_reuse_address = True

    server = AddressReuseTCPServer(('', PORT), ComplexHTTPRequestHandler)

    sys.stderr.write('Serving on port {0} ...\n'.format(PORT))
    server.serve_forever()

@task()
def reserve():
    """`build`, then `serve`"""
    build()
    serve()

@task()
def preview():
    """Build production version of site"""
    local('pelican -s publishconf.py')

@task()
def gh_pages():
    """Publish to GitHub Pages"""
    rebuild()
    local('cd $(OUTPUTDIR) && git add -A && git commit -m "Update pelican website" && git push origin master')
