#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Humberto Alves'
SITENAME = 'Meteolink • Blog'
SITEURL = 'https://blog.meteolink.eu'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Lisbon'

DEFAULT_LANG = 'PT'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# MENUITEMS = (('Archives', '/archives/'),
#              ('Categories', '/categories/'),
#              ('Tags', '/tags/'),)

# # Blogroll
LINKS = (('Meteolink', 'https://meteolink.eu'),
         ('Status', 'https://status.meteolink.eu'))

ICONS = (('gitlab', 'https://gitlab.com/meteolink'),
         # ('facebook', 'https://facebook.com/meteolink'),
         ('rss', SITEURL + '/feeds/main.atom.xml'),
         ('envelope', 'mailto:hello@meteolink.eu'))

# Social widget
SOCIAL = (
    ('github', 'https://github.com/hjalves'),
    ('linkedin', 'https://www.linkedin.com/in/hjalves/'),
    #('facebook', 'https://facebook.com/hjalves'),
    ('envelope-o', 'mailto:hello@hjalves.eu'),
    ('rss', SITEURL + '/feeds/atom.xml'),
)


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# THEME = './themes/aboutwilson'
THEME = './themes/alchemy'          # noice
# THEME = './themes/attila'
# THEME = './themes/blue-penguin'
# THEME = './themes/Casper2Pelican'
# THEME = './themes/chunk'
# THEME = './themes/clean-blog'
# THEME = './themes/genus'
# THEME = './themes/hyde'           # noice
# THEME = './themes/mediumfox'
# THEME = './themes/monospace'
# THEME = './themes/pelican-hss'
# THEME = './themes/pure'
# THEME = './themes/semantic-ui'
# THEME = './themes/simple-bootstrap'
